import Foundation

// Initial variables
let finalSquare: Int = 25
var numberOfPlayers: Int = 4
var hasWinner: Bool = false
var players: [(name: String, currentPosition: Int)] = []
var board = [Int](count: finalSquare + 1, repeatedValue: 0)
var roundNumber: Int = 0

// Board Setup & Initiliaze Player Profile
func boardSetup(){
    board[03] = +08; board[06] = +11; board[09] = +09; board[10] = +02
    board[14] = -10; board[19] = -11; board[22] = -02; board[24] = -08
    
    for i in 1...numberOfPlayers{
        players.append((name:"\(i)",currentPosition:0))
    }
}

// Dice Roll Function
func rollDice() -> Int {
    return Int(arc4random()) % 6+1
}

// Print Current Position Function
func printCurrentPosition(playerName: String,playerPosition: Int, rolledNumber: Int){
    print("Player \(playerName) is at square No.\(playerPosition) and he just rolled a number of \(rolledNumber).")
    
}


boardSetup()

while !hasWinner{
    roundNumber += 1
    print("This is round number \(roundNumber).")
    
    for i in 0..<numberOfPlayers{
        var newMove = rollDice()
        printCurrentPosition(players[i].name, playerPosition: players[i].currentPosition, rolledNumber: newMove)
        players[i].currentPosition += newMove
        
        if players[i].currentPosition < finalSquare{
            var boardMove = board[players[i].currentPosition]
            players[i].currentPosition += boardMove
            if boardMove > 0 {print("Cool, Player \(players[i].name) found a ladder, and climb up by \(boardMove).")}
                
            else {print("Too bad, Player \(players[i].name) kena snake, and move backward by \(boardMove).")
            }
        }
        
        if players[i].currentPosition > finalSquare {
            players[i].currentPosition = 2*finalSquare - (players[i].currentPosition)
            print("Player \(players[i].name) has reset to squre No. \(players[i].currentPosition)! \n")
        }
        
        if players[i].currentPosition == finalSquare {
            hasWinner = true
            print ("Player \(players[i].name) is now at squre No.\(players[i].currentPosition).")
            print("\nPLAYER \(players[i].name) HAS WON THE GAME")
            break
        }
            
        else{print ("Player \(players[i].name) is now at squre No.\(players[i].currentPosition). \n ")
        }
        
    }
}

