//: Playground - noun: a place where people can play

import UIKit

class Player {
    var name: String
    var position: Int = 0
init(playerName: String){
    self.name = playerName}
}


class gameDie {
    let numberofSide: Int
    init(Dice: Int) {
        self.numberofSide = Dice
    }
    func rollDice()-> Int {
       return Int(arc4random()) % self.numberofSide+1
    }
}

enum ActionType {
case Snake
case Ladder
}


class Board{
    let boxTarget: [Int: Int]
    let finalSquare: Int

    init(GameSize: Int, boxTarget: [Int: Int]){
        self.finalSquare = GameSize
        self.boxTarget = boxTarget
        }
    
    func moveFrom(position:Int, rolledNumber:Int)-> (finalposition: Int, moveType: ActionType?){
        let moveToPosition = position + rolledNumber
    
        if let actionPosition =  boxTarget[moveToPosition]{
        
            if actionPosition>moveToPosition{
                return (actionPosition, ActionType.Ladder)
            }
            else {return(actionPosition, ActionType.Snake)
            }
        }
        else {return(moveToPosition,nil)}
    }
}


class Game {
    let board: Board
    var players: [Player] = []
    var winner: Player?
    var die: gameDie
    private var roundNumber: Int = 0
    
    init(board: Board,rolledNumber: gameDie){
        
        self.board = board
        self.die = rolledNumber
    }
    

    func hasWinner()-> (Bool){
        return (winner != nil)
    }

    func runGame(){
        while !hasWinner(){
            roundNumber += 1
            print("###### ROUND NUMBER\(roundNumber) ######")
        
            for player in players{
                let rolledDice = die.rollDice()
                let (finalPosition, actionType) = board.moveFrom(player.position,rolledNumber: rolledDice)
                print("\(player.name) is at square No.\(player.position) and he just rolled a number of \(rolledDice).")
            
                if let actionType = actionType{
                    switch actionType{

                    case.Ladder: print("Cool,\(player.name) found a ladder, and climb up to \(finalPosition) .")
                    
                    case.Snake: print("Too bad,\(player.name) kena snake, and move backward to \(finalPosition) .")
                    }
                }
                player.position = finalPosition
                if player.position > board.finalSquare {
                player.position = 2*board.finalSquare - (player.position)
                print("\(player.name) has reset to squre No. \(player.position)! \n")
                }
                else if player.position == board.finalSquare {
                self.winner = player
                print ("\(player.name) is now at square No.\(player.position).")
                print("\n\(player.name) HAS WON THE GAME")
                break
                }
                else{print ("\(player.name) is now at square No.\(player.position). \n ")
                }
            }
        }
    }
}
//try
var playerone: Player = Player(playerName: "PC")
var playertwo: Player = Player(playerName: "Ivan")

var firstGame : Board = Board(GameSize: 100, boxTarget: [3:10, 8:5, 13:28, 15:1, 22:34, 50:97])
var go: Game = Game(board: firstGame, rolledNumber: gameDie(Dice: 6))

go.players.append(playerone)
    
go.players.append(playertwo)
go.runGame()
